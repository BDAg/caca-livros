const Book = require('../models/Book');

module.exports = {
  async show(req, res) {
    
    try {

      const books = await Book.find();

      res.status(200).send(books);

    } catch(err) {
      res.status(400).send({message: err});
    }

  }
}