const express = require('express');
const morgan = require('morgan');
const server = express();
const routes = require('./routes/bookRoute');
const mongoose = require('mongoose');

const port = process.env.PORT || 8081;

server.use(express.json());
server.use(morgan('dev'));

mongoose.connect("mongodb+srv://pinaradmin:pinar@root@clusterorigin.y0uq6.mongodb.net/myFirstDatabase?retryWrites=true&w=majority", { useNewUrlParser: true}, () => {
    console.log("\nBanco Conectado");
})

server.use(routes);

server.listen(port, ()=>{
    console.log('\033[1;31;43mservidor iniciado...\nOuvindo requisições....\033[m');
    console.log(`Porta: ${port}`);
})



