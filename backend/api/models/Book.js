var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BookSchema = new Schema({
    "upc": String,
    "title": String,
    "price": mongoose.Decimal128,
    "availability": Number,
    "category": String
}, { collection: 'books' });

module.exports = BookSchema;