const express = require('express');
const routes = express.Router();
const BookController = require('../controllers/bookController');

routes.get('/', (request, response) => {
    return response.send({Message:'Bem vindo a página inicial do projeto'});
})

routes.get('/books', BookController.show);

module.exports = routes;