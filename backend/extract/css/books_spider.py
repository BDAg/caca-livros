import scrapy

class BooksSpider(scrapy.Spider):
    name = 'books'

    start_urls = ['http://books.toscrape.com/index.html']

    def parse(self, response):

        book_page_links = response.css('.product_pod a')
        yield from response.follow_all(book_page_links, self.parse_book)

        pagination_links = response.css('.next a')
        yield from response.follow_all(pagination_links, self.parse)

    def parse_book(self, response):
        yield {
            'nome': response.css('div.col-sm-6.product_main h1::text').get(),
            'preco': response.css('p.price_color::text').get().replace("£",""),
            'estoque': response.css('table.table.table-striped td::text').getall()[5].replace("(","").split(" ")[2],
            'codigo': response.css('table.table.table-striped td::text').get(),
            'categoria': response.css('ul.breadcrumb a::text').getall()[2]
        }