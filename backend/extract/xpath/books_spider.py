import scrapy

class BooksSpider(scrapy.Spider):
    name = "books"
    max_pages = 50

    def start_requests(self):
        for i in range(self.max_pages):
            yield scrapy.Request(url="http://books.toscrape.com/catalogue/page-{0}.html".format(i), callback=self.parse)

    def parse(self, response):
        for link in response.xpath('//article[@class="product_pod"]/div/a/@href').extract():
            yield response.follow(link, callback=self.parse_detail)
    
    def parse_detail(self, response):
        upc = response.xpath('//th[contains(text(), "UPC")]/'
                             'following-sibling::td/text()').extract_first()
        img = response.xpath('//div[contains(@class, "thumbnail")]/div/div/img/@src').extract()
        img = [item.replace("../..", "http://books.toscrape.com") for item in img]
        title = response.xpath('//div[contains(@class, "product_main")]/h1/text()').extract_first()
        price = response.xpath('//div[contains(@class, "product_main")]/'
                               'p[@class="price_color"]/text()').extract_first().replace("£","")
        availability = response.xpath('//div[contains(@class, "product_main")]/'
                                      'p[contains(@class, "availability")]/text()').extract()
        availability = ''.join(availability).strip()
        availability = int(''.join(i for i in availability if i.isdigit()))

        category = response.xpath("//li[@class='active']/preceding::li[position() <= 1]/a/text()").extract_first()

        yield {
            'upc': upc,
            'img': img,
            'title': title,
            'price': price,
            'availability': availability,
            'category': category
        }