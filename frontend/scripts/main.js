const dataObjectArray = [];
const lowerToHighest = [];
const highestToLower= [];
const URL = "https://bookhunter-api.herokuapp.com/books/list/all";

function getAllData() {
  document.getElementById("funciona").innerHTML = "";
  fetch(URL)
      .then(response => response.json())
      .then(result => {
          var i = 0;
          result.forEach(function(data) {
              dataObjectArray.push(data);
              tabelaElemento(dataObjectArray[i].img, dataObjectArray[i].title, dataObjectArray[i].price);
              i = i+1;
          });
          
      })
      .catch(function(err){
          console.error(err);
      })
};

function filter_lower_to_highest(){
    document.getElementById("funciona").innerHTML = "";
    fetch(URL)
      .then(response => response.json())
      .then(result => {
          result.forEach(function(data) {
            lowerToHighest.push(data);
            lowerToHighest.sort((a,b)=>parseFloat(a.price) - parseFloat(b.price));
          });
          lowerToHighest.map(x => tabelaElemento(x.img, x.title, x.price))
      })
      .catch(function(err){
          console.error(err);
      })
}

function filter_highest_to_lower(){
  document.getElementById("funciona").innerHTML = "";
  fetch(URL)
    .then(response => response.json())
    .then(result => {
        result.forEach(function(data) {
          highestToLower.push(data);
          highestToLower.sort((a,b)=>parseFloat(b.price) - parseFloat(a.price));
        });
        highestToLower.map(x => tabelaElemento(x.img, x.title, x.price))
        
    })
    .catch(function(err){
        console.error(err);
    })
}

var btnContainer = document.getElementById("filter-button");
var btns = btnContainer.getElementsByClassName("btn");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
}

function tabelaElemento(img, title, price) {
  const bookEl = document.createElement("div");

  const bookInnerHTML = `
      <div class="col-sm-4" id="item" style="height:500px;">
          <div class="shop-item">
              <div class="photo">
                  <img src="${img}" class="img-responsive" alt="a"/>
              </div>
              <div class="info">
                  <div class="row">
                      <div class="price col-md-6">
                          <h5>
                              ${title}</h5>
                          <h5 class="price-text-color">
                              R$ ${price}</h5>
                      </div>
                      <div class="rating hidden-sm col-md-6">
                          <i class="price-text-color fa fa-star"></i><i class="price-text-color fa fa-star">
                      </i><i class="price-text-color fa fa-star"></i><i class="price-text-color fa fa-star">
                      </i><i class="fa fa-star"></i>
                      </div>
                  </div>
                  <div class="separator clear-left">
                      <p class="btn-add">
                          <i class="fa fa-shopping-cart"></i><a href="http://www.jquery2dotnet.com"
                                                              class="hidden-sm">Livraria Cultura</a></p>

                      <p class="btn-details">
                          <i class="fa fa-list"></i><a href="produto.html" class="hidden-sm">Mais
                              Detalhes</a></p>
                  </div>
                  <div class="clearfix">
                  </div>
              </div>
          </div>
      </div>
  `;

  bookEl.innerHTML = bookInnerHTML;

  funciona.appendChild(bookEl);
}

function myFunction() {
    var inp, filter, items, h5, i, txtValue;
    inp = document.getElementById("form1");

    inp.addEventListener("input", function(e) {
        filter = inp.value.toUpperCase();
        items = document.querySelectorAll("[id='item']");
        for (i = 0; i < items.length; i++) {
            h5 = items[i].getElementsByTagName("h5")[0];
            txtValue = h5.textContent || h5.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                items[i].style.display = "";
            } else {
                items[i].style.display = "none";
            }
        }
    });
}

getAllData();
myFunction();